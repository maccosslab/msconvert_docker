FROM microsoft/dotnet-framework:3.5-windowsservercore-ltsc2016

COPY pwiz-setup-3.0.11579-x86_64.msi /INSTALLERS/pwiz-setup-3.0.11579-x86_64.msi
COPY Miniconda2-latest-Windows-x86_64.exe /INSTALLERS/Miniconda2-latest-Windows-x86_64.exe

# install proteowizard
RUN msiexec.exe /i "C:\INSTALLERS\pwiz-setup-3.0.11579-x86_64.msi" /qn

# install miniconda
RUN /INSTALLERS/Miniconda2-latest-Windows-x86_64.exe /S /AddToPath=0 /D=C:\Program Files\miniconda2
COPY set_path.ps1 /INSTALLERS/set_path.ps1
RUN powershell -Command c:\INSTALLERS\set_path.ps1
ENV PYTHONIOENCODING=UTF-8

# install boto3
RUN pip install botocore==1.8.27
RUN pip install boto3==1.5.13
RUN mkdir "C:\tmp"
ENV SCRATCH_DIR="C:\tmp"
COPY s3_wrap.py  /app/s3_wrap.py
ENTRYPOINT ["C:\\Program Files\\miniconda2\\python.exe", "C:\\app\\s3_wrap.py", "C:\\Program Files\\ProteoWizard\\ProteoWizard 3.0.11579\\msconvert.exe"]
