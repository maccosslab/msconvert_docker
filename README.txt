Build the container:
docker build . -t msconvert_dock

Run the container, mounting a local host directory:
docker run --mount type=bind,source="C:\Users\jegertso\docker_dump",target="C:\data"  msconvert_dock data/*.raw --outdir data --filter "peakPicking vendor msLevel=1-2" --mzML