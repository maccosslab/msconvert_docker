"""
Copyright 2017 Jarrett D. Egertson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

"""
This code is derived from this source code written by Jake Feala (@jfeala) for Outlier Bio (@outlierbio):
https://github.com/outlierbio/ob-pipelines/blob/master/ob_pipelines/s3.py.
The above copyright covers modifications to the referenced code.
"""

import argparse
from functools import wraps
import logging
import os
import os.path as op
import shutil
from subprocess import check_output, CalledProcessError, STDOUT
import sys
from tempfile import mkdtemp, mkstemp
try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

import boto3
import botocore
from botocore.exceptions import ClientError

logger = logging.getLogger('s3-wrap')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stderr))

s3 = boto3.client('s3')
s3_resource = boto3.resource('s3')

SCRATCH_DIR = os.environ.get('SCRATCH_DIR') or '/tmp'


def create_tmp_from_key(key):
    """Create local temp file or folder depending on key path"""
    return op.join(SCRATCH_DIR, os.path.normpath(key))


def path_to_bucket_and_key(path):
    (scheme, netloc, path, params, query, fragment) = urlparse(path)
    path_without_initial_slash = path[1:]
    return netloc, path_without_initial_slash


def key_exists(bucket, key):
    """Check for existence of S3 key"""
    try:
        if key.endswith('/'):
            return 'Contents' in s3.list_objects(Bucket=bucket, Prefix=key)
        else:
            s3_resource.Object(bucket, key).load()
            return True
    except botocore.exceptions.ClientError as e:
        return False


def download_file(bucket, key, local_path):
    """
    :param bucket: s3 bucket to download the file from
    :param key: s3 key for the file
    :param local_path: path to download the file to
    :return:
    """
    p = op.dirname(local_path)
    if not op.exists(p):
        os.makedirs(p)
    logger.info("Downloading {} {} {}".format(bucket, key, local_path))
    s3.download_file(bucket, key, local_path)


def download_folder(bucket, prefix, folder):
    response = s3.list_objects(
        Bucket=bucket,
        Prefix=prefix,
        Delimiter='/'
    )
    if 'Contents' not in response and response['HTTPStatusCode'] == 200:
        raise botocore.exceptions.ClientError
    for key_dict in response['Contents']:
        key = key_dict['Key']
        fpath = op.join(folder, op.basename(key))
        if op.exists(fpath):
            continue
        if key.endswith('/'):
            os.makedirs(fpath)
        else:
            print "Downloading: {} {} {}".format(bucket, key, fpath)
            s3.download_file(bucket, key, fpath)


def upload_folder(folder, bucket, prefix):
    # Upload directory
    for fname in os.listdir(folder):
        fpath = op.join(folder, fname)
        key = prefix + fname
        s3.upload_file(fpath, bucket, key)


def download_file_or_folder(s3_path, local_path):
    """Dispatch S3 download depending on key path"""
    print "download file or folder: {} {}".format(s3_path, local_path)
    bucket, key = path_to_bucket_and_key(s3_path)
    print "download file or folder buck and key: {} {}".format(bucket, key)
    if key.endswith('/'):
        download_folder(bucket, key, local_path)
    else:
        download_file(bucket, key, local_path)


def upload_file_or_folder(s3_path, local_path):
    """Dispatch S3 upload depending on local path"""
    bucket, key = path_to_bucket_and_key(s3_path)
    logger.info("Uploading {} -> {} {}".format(local_path, bucket, key))
    if op.isfile(local_path):
        s3.upload_file(local_path, bucket, key)
    elif op.isdir(local_path):
        if not key.endswith('/'):
            key += '/'
        upload_folder(local_path, bucket, key)


def remove_file_or_folder(fpath):
    if op.isdir(fpath):
        shutil.rmtree(fpath)
    else:
        os.remove(fpath)


def swap_args(args, rm_local_outpath=False):
    """Swap S3 paths in arguments with local paths

    If the S3 path exists, it's an input, download first and swap the arg
    with a temporary filepath. Otherwise, it's an output, save for upload
    after the command.

    Returns:
        tuple of (local_args, s3_downloads, s3_uploads)

        where new_args contains the new argument list with local paths,
        and s3_outputs is a dict mapping local filepaths to s3 paths to
        transfer after execution.
    """
    s3_uploads = {}
    s3_downloads = {}
    local_args = []
    for arg in args:
        if not arg.startswith('s3://'):
            local_args.append(arg)
            continue

        src_bucket, src_key = path_to_bucket_and_key(arg)
        local_tmp = create_tmp_from_key(src_key)

        # If key exists, add path to downloads, otherwise add path to
        # uploads and remove the file or folder
        if key_exists(src_bucket, src_key):
            s3_downloads[arg] = local_tmp
        else:
            s3_uploads[arg] = local_tmp
            if rm_local_outpath:
                remove_file_or_folder(local_tmp)

        local_args.append(local_tmp)

    add_additional_downloads_and_uploads(s3_downloads, s3_uploads)
    return local_args, s3_downloads, s3_uploads


def add_additional_downloads_and_uploads(s3_downloads, s3_uploads):
    """
    Process environment variables specifying additional s3 paths to downloads/uploads which are not specified on the
    command line by explicit arguments. Checks environment variables s3wrap_extra_downloads and s3wrap_extra_uploads
     for additional s3 paths to download/upload. S3 paths should be full paths, without the s3:// prefix. For example,
     an s3 path in bucket pathbucket with key path/to/file.txt would be passed as pathbucket/path/to/file.txt .
     Use a colon delimiter to specify multiple paths
    :param s3_downloads: existing s3_downloads dictionary to append new paths to
    :param s3_uploads: existing s3_uploads dictionary to append new paths to
    :return: None
    """
    extra_downloads = list()
    if 's3wrap_extra_downloads' in os.environ:
        extra_downloads = ['s3://' + p for p in os.environ['s3wrap_extra_downloads'].split(':')]

    extra_uploads = list()
    if 's3wrap_extra_uploads' in os.environ:
        extra_uploads = ['s3://' + p for p in os.environ['s3wrap_extra_uploads'].split(':')]

    if 's3wrap_file_lists' in os.environ:
        # process a list of files to download from s3 that themselves are list of s3 paths which will also
        # be downloaded and converted to local files
        temp_download_dir = mkdtemp(dir=SCRATCH_DIR)
        temp_dl_file = os.path.join(temp_download_dir, 'temp_file_list.txt')
        for f_list in os.environ['s3wrap_file_lists'].split(':'):
            fl_path = 's3://' + f_list
            if fl_path not in s3_downloads:
                extra_downloads.append(fl_path)
            f_bucket, f_key = path_to_bucket_and_key(fl_path)
            # download the list file to a temporary location and add s3 paths it contains to the s3_downloads
            # location
            download_file(f_bucket, f_key, temp_dl_file)
            with open(temp_dl_file) as fin:
                for line in fin:
                    s3_p = line.strip()
                    if s3_p.startswith('s3://'):
                        extra_downloads.append(s3_p)
            os.remove(temp_dl_file)
        os.rmdir(temp_download_dir)

    for p in extra_downloads:
        src_bucket, src_key = path_to_bucket_and_key(p)
        local_tmp = create_tmp_from_key(src_key)
        s3_downloads[p] = local_tmp

    for p in extra_uploads:
        src_bucket, src_key = path_to_bucket_and_key(p)
        local_tmp = create_tmp_from_key(src_key)
        s3_uploads[p] = local_tmp


def s3args(rm_local_outpath=False, mkdir_for_upload=True):
    """Sync S3 path arguments with behind-the-scenes S3 transfers

    When decorating a function, s3args downloads all arguments that
    look like S3 paths to temporary files and swaps the local temp
    filepath as the new argument. If the S3 path does not exist, it
    is assumed to be an output, and s3args uploads the tempfile
    back to S3 after the command is complete.

    This works great with Luigi, which checks for existence of inputs
    and non-existence of outputs before running a Task.

    Keyword args are passed directly without syncing, for now.
    """
    def s3args_decorator(f):
        @wraps(f)
        def local_fn(*args, **kwargs):

            # Swap the S3 path arguments for local temporary files/folders
            local_args, s3_downloads, s3_uploads = swap_args(args, rm_local_outpath=rm_local_outpath)
            print "local args", local_args
            print "s3_downloads", s3_downloads
            print "s3_uploads", s3_uploads

            try:
                # Download inputs
                logger.info('syncing from S3 new')
                for s3_path, local_path in s3_downloads.items():
                    print s3_path
                    download_file_or_folder(s3_path, local_path)

                if 's3wrap_file_lists' in os.environ:
                    # for each file listing, replace the list of s3 paths with a list of
                    # converted local paths
                    for f_list in os.environ['s3wrap_file_lists'].split(':'):
                        fl_path = 's3://' + f_list
                        local_path = s3_downloads[fl_path]
                        fd, temp_path = mkstemp(dir=SCRATCH_DIR)
                        with os.fdopen(fd, 'w') as fout, open(local_path) as fin:
                            for line in fin:
                                ls = line.strip()
                                if ls.startswith('s3://'):
                                    fout.write(s3_downloads[ls] + os.linesep)
                                else:
                                    fout.write(line + os.linesep)
                        os.remove(local_path)
                        shutil.move(temp_path, local_path)

                if mkdir_for_upload:
                    for s3_path, local_path in s3_uploads.items():
                        # create any necessary upload directories for s3
                        dir_name = os.path.dirname(local_path)
                        if not os.path.exists(dir_name):
                            os.makedirs(dir_name)

                # Run command and save output
                out = f(*local_args, **kwargs)

                # Upload outputs
                logger.info('uploading to S3')
                for s3_path, local_path in s3_uploads.items():
                    upload_file_or_folder(s3_path, local_path)
            finally:
                # Remove local files
                local_paths = list(s3_downloads.values()) + list(s3_uploads.values())
                for local_path in local_paths:
                    try:
                        remove_file_or_folder(local_path)
                    except OSError:
                        pass
            return out

        return local_fn
    return s3args_decorator


def s3wrap():
    parser = argparse.ArgumentParser(description='Swap S3 commands for temporary local paths and download')
    parser.add_argument('--rm-local-outpath', '-r', action='store_true',
                        help='Remove local tmp output file/folder before executing command')
    parser.add_argument('--skip-mkdir-for-upload', '-s', action='store_true',
                        help='Skip making a local sub directories for requested upload files')
    parser.add_argument('command', nargs=argparse.REMAINDER)
    args = parser.parse_args()

    @s3args(rm_local_outpath=args.rm_local_outpath, mkdir_for_upload=not args.skip_mkdir_for_upload)
    def sync_and_run(*cmds):
        print('Running:\n{}'.format(' '.join(cmds)))
        try:
            command_out = check_output(cmds, stderr=STDOUT)
        except CalledProcessError as err:
            print err.output
            raise
        return command_out

    out = sync_and_run(*args.command)
    print(out)

if __name__ == '__main__':
    s3wrap()
